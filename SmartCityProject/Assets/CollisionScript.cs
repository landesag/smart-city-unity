﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Collections.Generic;
public class ResponseClass
{
    public float data;

}
public class CollisionScript : MonoBehaviour {

    ArrayList peopleWaiting;

    private string tramName;
    IEnumerator RequestTimeToWebService()
    {
        while (true)
        {
            WWW w = new WWW("http://localhost:8080/1.0/trams/" + tramName +"/time?x="+transform.position.x+"&z="+transform.position.z);
            yield return w;
            yield return new WaitForSeconds(1f);
            Transform cartel = transform.GetChild(0);
            TextMesh textMeshOfCartel = cartel.gameObject.GetComponent<TextMesh>();
            string[] stringsOfText = textMeshOfCartel.text.Split(':');

            ResponseClass response = JsonUtility.FromJson<ResponseClass>(w.text);

            textMeshOfCartel.text = stringsOfText[0] + ": " + response.data;

            yield return new WaitForSeconds(1.5f);

        }

    }
    void Start()
    {
        peopleWaiting = new ArrayList();
        if (transform.position.z < 60)
        {
            tramName = "Tram";
        }else
        {
            tramName = "Tram2";
        }

        StartCoroutine("RequestTimeToWebService");

    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name.Equals("Tram") || collider.gameObject.name.Equals("Tram2"))
        {

            Transform cartel = transform.GetChild(0);
            TextMesh textMeshOfCartel = cartel.gameObject.GetComponent<TextMesh>();
            string[] stringsOfText = textMeshOfCartel.text.Split(':');
            textMeshOfCartel.text = stringsOfText[0] + ": Saliendo";

            Debug.Log("Waiting " + peopleWaiting.Count + " people");
            MoveScript scriptTram = collider.GetComponent<MoveScript>();
            if (peopleWaiting.Count > 0)
            {
                scriptTram.speed = 0;
                GameObject person;
                for (int i = peopleWaiting.Count - 1; i >= 0; i--)
                {
                    person = (GameObject)peopleWaiting[i];
                    person.GetComponent<MovePersonScript>().actualTarget = collider.gameObject.transform;
                    person.GetComponent<MovePersonScript>().actualTargetObject = collider.gameObject;
                    person.GetComponent<MovePersonScript>().doing = "entrarEnTranvia";
                    person.transform.parent = collider.transform; // Se hace que el personaje vaya pegado al tranvía
                    scriptTram.peopleInside.Add(person);
                    scriptTram.nombresMarquesinasPendientes.Add(person.GetComponent<MovePersonScript>().nombreMarquesinaDestino);
                    peopleWaiting.Remove(peopleWaiting[i]);

                }
                scriptTram.stopped = true;
                
            }

            if(scriptTram.peopleInside.Count > 0 && scriptTram.stopped == false && scriptTram.nombresMarquesinasPendientes.Contains(this.name)){
                scriptTram.speed = 0;
                GameObject person;

                for (int i = scriptTram.peopleInside.Count - 1; i >= 0; i--)
                {
                    person = (GameObject)scriptTram.peopleInside[i];
                    MovePersonScript personScript = person.GetComponent<MovePersonScript>();
                    if (personScript.nombreMarquesinaDestino.Equals(this.name)){
                        person.transform.parent = null;
                        scriptTram.peopleInside.Remove(person);
                        personScript.actualTarget = personScript.targetObject.transform;
                        personScript.actualTargetObject = personScript.targetObject;
                        person.GetComponent<MovePersonScript>().doing = "irAlEdificio";
                    }
                }
                scriptTram.speed = 50;
            }
        }


        if (collider.gameObject.tag.Equals("USCperson") || collider.gameObject.tag.Equals("person") 
            || collider.gameObject.tag.Equals("USCperson2"))
        {
            if (collider.gameObject.GetComponent<MovePersonScript>().doing.Equals("avanzarAMarquesina"))
            {
                peopleWaiting.Add(collider.gameObject);
            }
        }

    }


    void OnTriggerExit(Collider collider)
    {

        if (collider.gameObject.tag.Equals("USCperson"))
        {

            string name = collider.name;
            GameObject person;
            for (int i = peopleWaiting.Count - 1; i >= 0; i--)
            {
                person = (GameObject)peopleWaiting[i];
                if (person.name == name)
                    peopleWaiting.Remove(peopleWaiting[i]);

            }

        }
    }
}
