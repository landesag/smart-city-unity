﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class BagScript : MonoBehaviour {


    IEnumerator SendPositionToWebService()
    {
        while (true)
        {
            Position position = new Position(transform.position.x, transform.position.z);
            string parentName;
            if (transform.parent != null)
            {
                parentName = transform.parent.name;
            }else
            {
                parentName = "LOST";
            }
            string url = "http://localhost:8080/1.0/bags/" + this.name + "?dueno=" + parentName;
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");

            string json = JsonUtility.ToJson(position);
            byte[] bytes = Encoding.UTF8.GetBytes(json);
            WWW www = new WWW(url, bytes, headers);

            yield return www;
            yield return new WaitForSeconds(2);

        }

    }

    // Use this for initialization
    void Start () {

        StartCoroutine("SendPositionToWebService");

    }

    // Update is called once per frame
    void Update () {
	
	}


    public void Perder()
    {
        transform.parent = transform.parent.parent;
    }
}
