﻿using UnityEngine;
using System.Collections;

public class MovePersonScript : MonoBehaviour
{
    private Transform target;
    public GameObject targetObject;

    public Transform actualTarget;
    public GameObject actualTargetObject;

    public string nombreMarquesinaDestino;
    private string aceraActual;
    private string aceraObjetivo;
    public string doing;
    private bool canRestart = true;

    public float speed;

    public void Init()
    {


        target = targetObject.transform;

        if (transform.position.z < 50)
        {
            Debug.Log("Acera inicial : B");
            aceraActual = "B";
        }

        if (transform.position.z > 75)
        {
            Debug.Log("Acera inicial : A");
            aceraActual = "A";
        }


        if (target.position.z < 50)
        {
            Debug.Log("Acera inicial objetivo : B");
            aceraObjetivo = "B";
        }

        if (target.position.z > 75)
        {
            Debug.Log("Acera inicial objetivo : A");
            aceraObjetivo = "A";
        }



        if (aceraActual != aceraObjetivo) //Se busca primero un paso subterraneo para cruzar si las aceras no coniciden
        {
            GameObject[] marcas;
            GameObject marcaObjetivo = null;

            string tag = "marcaSubterraneo" + aceraActual; //Se buscan los pasos de esa acera 
            marcas = GameObject.FindGameObjectsWithTag(tag);
            float lowestDistance = 9999999999999;

            foreach (GameObject marca in marcas)
            {
                float distance = Vector3.Distance(marca.transform.position, transform.position);
                if (distance < lowestDistance)
                {
                    marcaObjetivo = marca;
                    lowestDistance = distance;
                }
            }

            //Una vez encontrado la marca más cercana se establece como objetivo actual

            actualTarget = marcaObjetivo.transform;
            actualTargetObject = marcaObjetivo;

            doing = "avanzarAMarcaMasCercana";
        }
        else if (Vector3.Distance(transform.position, target.position) < 100) //Si el edificio esta cerca se va
        {
            doing = "avanzarAEdificio";
            actualTarget = target;
        }
        else //Si no esta cerca y es la misma acera se va a la marquesina mas cercana
        {


            //Se calcula antes de nada en que marquesina se quiere parar obteniendo todas las de esa acera y buscando la mas cercana al objetivo
            string tag = "marquesina" + aceraActual; //Se buscan las marquesinas de esa acera
            GameObject[] marquesinasTarget;
            GameObject marquesinaMasCercanaATarget = null;
            marquesinasTarget = GameObject.FindGameObjectsWithTag(tag);
            float lowestDistance = 9999999999999;

            foreach (GameObject marquesina in marquesinasTarget)
            {
                float dist = Vector3.Distance(marquesina.transform.position, target.transform.position);
                if (dist < lowestDistance)
                {
                    marquesinaMasCercanaATarget = marquesina;
                    lowestDistance = dist;
                }
            }

            nombreMarquesinaDestino = marquesinaMasCercanaATarget.name; //Se almacena el nombre
            doing = "avanzarAMarquesina";
            lowestDistance = 9999999999999;

            GameObject[] marquesinas;
            GameObject marquesinaObjetivo = null;
            marquesinas = GameObject.FindGameObjectsWithTag(tag);

            foreach (GameObject marquesina in marquesinas)
            {
                float dist = Vector3.Distance(marquesina.transform.position, transform.position);
                if (dist < lowestDistance)
                {
                    marquesinaObjetivo = marquesina;
                    lowestDistance = dist;
                }
            }

            actualTarget = marquesinaObjetivo.transform;
            actualTargetObject = marquesinaObjetivo;
        }

    }

    GameObject obtenerMarcaContraria(GameObject marcaActual)
    {
        if (marcaActual.name.Equals("MarcaSubterraneo1.1"))
        {
            return GameObject.Find("MarcaSubterraneo1.2");
        }
        if (marcaActual.name.Equals("MarcaSubterraneo1.2"))
        {
            return GameObject.Find("MarcaSubterraneo1.1");
        }
        if (marcaActual.name.Equals("MarcaSubterraneo2.1"))
        {
            return GameObject.Find("MarcaSubterraneo2.2");
        }
        if (marcaActual.name.Equals("MarcaSubterraneo2.2"))
        {
            return GameObject.Find("MarcaSubterraneo2.1");
        }
        if (marcaActual.name.Equals("MarcaSubterraneo3.1"))
        {
            return GameObject.Find("MarcaSubterraneo3.2");
        }
        if (marcaActual.name.Equals("MarcaSubterraneo3.2"))
        {
            return GameObject.Find("MarcaSubterraneo3.1");
        }
        return null;

    }
    void Start()
    {

        Init();
    }
    void Update()
    {

        GameObject daynight = GameObject.Find("DayNight");
        DayNightController dnc = daynight.GetComponent<DayNightController>();

        if (dnc.currentTimeOfDay > 0.85 && canRestart)
        {
            string newBuildingString = "Building" + Random.Range(1, 3);
            GameObject newBuilding = GameObject.Find(newBuildingString);
            targetObject = newBuilding;
            canRestart = false;
            Init();
        }

        float step = speed * Time.deltaTime;
        Vector3 targetPosition = new Vector3(actualTarget.position.x, 0, actualTarget.position.z);

        transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);


        if (doing.Equals("avanzarAMarcaMasCercana") && actualTarget.position == transform.position) //Llegó al paso subterraneo
        {
            doing = "avanzarAMarcaContraria";
            obtenerMarcaContraria(actualTargetObject);
            actualTargetObject = obtenerMarcaContraria(actualTargetObject);
            actualTarget = actualTargetObject.transform;
        }

        if (doing.Equals("avanzarAMarcaContraria") && actualTarget.position == transform.position) //Llegó a la otra acera
        {
            if (aceraActual.Equals("A"))
            {
                aceraActual = "B";
            }else
            {
                aceraActual = "A";
            }
            
            float distance = Vector3.Distance(transform.position, target.position);
            if (distance < 100) //Si el edificio esta cerca se va directamente
            {
                doing = "avanzarAEdificio";
                actualTarget = target;
            }else { //Si no se avanza a la marquesina para ir al tranvía

                //Se calcula antes de nada en que marquesina se quiere parar obteniendo todas las de esa acera y buscando la mas cercana al objetivo
                string tag = "marquesina" + aceraActual; //Se buscan las marquesinas de esa acera
                GameObject[] marquesinasTarget;
                GameObject marquesinaMasCercanaATarget = null;
                marquesinasTarget = GameObject.FindGameObjectsWithTag(tag);
                float lowestDistance = 9999999999999;

                foreach (GameObject marquesina in marquesinasTarget)
                {
                    float dist = Vector3.Distance(marquesina.transform.position, target.transform.position);
                    if (dist < lowestDistance)
                    {
                        marquesinaMasCercanaATarget = marquesina;
                        lowestDistance = dist;
                    }
                }

                nombreMarquesinaDestino = marquesinaMasCercanaATarget.name; //Se almacena el nombre

                doing = "avanzarAMarquesina";
                GameObject[] marquesinas;
                GameObject marquesinaObjetivo = null;
                marquesinas = GameObject.FindGameObjectsWithTag(tag);
                lowestDistance = 9999999999999;

                foreach (GameObject marquesina in marquesinas)
                {
                    float dist = Vector3.Distance(marquesina.transform.position, transform.position);
                    if (dist < lowestDistance)
                    {
                        marquesinaObjetivo = marquesina;
                        lowestDistance = dist;
                    }
                }

                actualTarget = marquesinaObjetivo.transform;
                actualTargetObject = marquesinaObjetivo;
            }
        }
    }
    
}