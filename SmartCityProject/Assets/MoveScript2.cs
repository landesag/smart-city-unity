﻿using UnityEngine;
using System.Collections;

public class MoveScript2 : MonoBehaviour
{
    public Transform target;
    public float speed;
    void Update()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject == GameObject.Find("MarcaCircuito2.1"))
        {
            target = GameObject.Find("MarcaCircuito2.2").transform;
        }

        if (collision.gameObject == GameObject.Find("MarcaCircuito2.2"))
        {
            target = GameObject.Find("MarcaCircuito2.1").transform;
        }

        
    }
}