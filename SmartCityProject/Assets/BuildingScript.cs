﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;


public class BuildingScript : MonoBehaviour
{
    private string securityIssuePersonName;

    public int securityLevel;

    IEnumerator SendSecurityIssue()
    {
        string url = "http://localhost:8080/1.0/security/problem/" + this.name + "?person="+securityIssuePersonName;
        WWW www = new WWW(url);
        yield return www;
        yield return new WaitForSeconds(1f);

        yield return new WaitForSeconds(3);
    }

    void OnTriggerEnter(Collider collider)
    {
        if (this.securityLevel == 1 && collider.tag.Equals("person"))
        {
            Debug.Log("alerta de seguridad");
            securityIssuePersonName = collider.name;
            StartCoroutine("SendSecurityIssue");

        }

        if (this.securityLevel == 2 && (collider.tag.Equals("person") || collider.tag.Equals("USCperson")))
        {
            Debug.Log("alerta de seguridad grave");
            securityIssuePersonName = collider.name;
            StartCoroutine("SendSecurityIssue");

        }

    }
}
