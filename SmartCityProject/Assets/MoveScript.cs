﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Collections.Generic;

public class Position
{
    public float x;
    public float z;
    public Position(float x_pos, float z_pos)
    {
        x = x_pos;
        z = z_pos;
    }
}
public class MoveScript : MonoBehaviour
{
    public Transform target;
    public float speed;
    public bool stopped;
    private bool keepGoing;

    public ArrayList peopleInside;
    public ArrayList nombresMarquesinasPendientes;

    IEnumerator AskWebservice()
    {
 

        WWW w = new WWW("http://localhost:8080/1.0/trams/Tram");
        yield return w;
        yield return new WaitForSeconds(1f);
        Debug.Log(w.text);

    }


    IEnumerator SendSecurityIssue()
    {
        string url = "http://localhost:8080/1.0/trams/problem/" + this.name;
        WWW www = new WWW(url);
        yield return www;
        yield return new WaitForSeconds(1f);

        yield return new WaitForSeconds(3);
    }

    IEnumerator SendPositionToWebService()
    {
        while (true)
        {
            Position position = new Position(transform.position.x, transform.position.z);
            string url = "http://localhost:8080/1.0/trams/" + this.name;
            Dictionary<string, string> headers = new Dictionary<string, string>();
            headers.Add("Content-Type", "application/json");

            string json = JsonUtility.ToJson(position);
            byte[] bytes = Encoding.UTF8.GetBytes(json);
            WWW www = new WWW(url, bytes, headers);

            yield return www;
            yield return new WaitForSeconds(1f);

        }

    }
    void Start()
    {
        peopleInside = new ArrayList();
        nombresMarquesinasPendientes = new ArrayList();

        StartCoroutine("SendPositionToWebService");
    }

    public void createObstacle()
    {

        GameObject cube = Instantiate(Resources.Load("ObstaculoPrefab")) as GameObject;
        cube.transform.position = new Vector3(323, 3, 65);

    }



    void Update()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        if (stopped)
        {

            keepGoing = true;
            GameObject person;
            for (int i = peopleInside.Count - 1; i >= 0; i--)
            {
                person = (GameObject)peopleInside[i];
                if (person.transform.position.x != transform.position.x || person.transform.position.z != transform.position.z)
                {
                    keepGoing = false;
                }

            }

            if(keepGoing)
            {
                speed = 50;
                stopped = false;
            }


        }

        GameObject daynight = GameObject.Find("DayNight");
        DayNightController dnc = daynight.GetComponent<DayNightController>();

        if (dnc.currentTimeOfDay > 0.24 && dnc.currentTimeOfDay < 0.85)
        {
            Transform foco = transform.GetChild(0);
            Light luzFoco = foco.GetComponent<Light>();
            luzFoco.enabled = false;

        }
        else
        {
            Transform foco = transform.GetChild(0);
            Light luzFoco = foco.GetComponent<Light>();
            luzFoco.enabled = true;

        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag.Equals("obstaculo"))
        {

            speed = 0;

            StartCoroutine("SendSecurityIssue");


        }

        if (collision.gameObject == GameObject.Find("MarcaCircuito1.1"))
        {
            target = GameObject.Find("MarcaCircuito1.2").transform;
        }

        if (collision.gameObject == GameObject.Find("MarcaCircuito1.2"))
        {
            target = GameObject.Find("MarcaCircuito1.3").transform;
        }


        if (collision.gameObject == GameObject.Find("MarcaCircuito1.3"))
        {
            target = GameObject.Find("MarcaCircuito1.4").transform;
        }


        if (collision.gameObject == GameObject.Find("MarcaCircuito1.4"))
        {
            target = GameObject.Find("MarcaCircuito1.1").transform;
        }

        if (collision.gameObject == GameObject.Find("MarcaCircuito2.1"))
        {
            target = GameObject.Find("MarcaCircuito2.2").transform;
        }

        if (collision.gameObject == GameObject.Find("MarcaCircuito2.2"))
        {
            target = GameObject.Find("MarcaCircuito2.1").transform;

        }

    }
}